module Set (Set, emptyS, addS, belongs, sizeS, removeS, unionS, intersectionS, setToList) where

import Data.List

data Set a = Set [a] Int
	deriving(Show)
--INVREP: NoHayRepetidosEn[a] y IntEsIgualALaCantidadDeElementosDe[a]

--OrdenConstante(O(1))
emptyS :: Set a
emptyS = (Set [] 0)

--OrdenLineal(O(n))
addS :: Eq a => a -> Set a -> Set a
addS x (Set ys n) =
	if elem x ys
		then (Set ys n)
		else (Set (x:ys) (n+1))

--OrdenConstante(O(1))
belongs :: Eq a => a -> Set a -> Bool
belongs x (Set ys n) = elem x ys

--OrdenConstante(O(1))
sizeS :: Eq a => Set a -> Int
sizeS (Set xs n) = n

--OrdenLineal(O(n+n))
removeS :: Eq a => a -> Set a -> Set a
removeS x (Set ys n) = (Set (delete x ys) (n-1) )

--OrdenCuadratico(O(n*n))
unionS :: Eq a => Set a -> Set a -> Set a
unionS (Set [] 0) (Set ys ny) = (Set ys ny)
unionS (Set (x:xs) nx) (Set ys ny) =
	if elem x ys
		then unionS (Set xs (nx-1)) (Set ys ny)
		else unionS (Set xs (nx-1)) (Set (x:ys) (ny+1))

--OrdenCuadratico(O(n*n))
intersectionS :: Eq a => Set a -> Set a -> Set a
intersectionS (Set xs nx) (Set ys ny) =
	(Set (intersection xs ys) (length (intersection xs ys)))

--OrdenConstante(O(1))
setToList :: Eq a => Set a -> [a]
setToList (Set xs n) = xs

--auxiliar
intersection :: Eq a => [a] -> [a] -> [a]
intersection [] ys = ys
intersection (x:xs) ys =
	if elem x ys
		then x:(intersection xs ys)
		else intersection xs ys
